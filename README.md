# reactive

#### 介紹
vue响应式原理

#### 架構
reactive文件源码

index响应效果

#### 实现过程：

观察者设计模式，一对多的方式，发布者响应类，观察者订阅类；

1. 定义类myVue，保存data数据，并设置this代理以便访问
2. 将数据添加到响应式系统 observer， el添加到compiler渲染
3. observer 监听data数据的变化，compiler寻找不同节点，子节点递归继续寻找
4. observer创建dep记录和添加watcher，compiler创建watcher订阅数据变化
5. 数据改变时，dep通知watcher，watcher更新节点信息

